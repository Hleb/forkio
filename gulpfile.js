const gulp = require('gulp');
const sass = require("gulp-sass");
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');
const autoPrefixer = require('gulp-autoprefixer');
const rename= require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const terser = require('gulp-terser');

function style() {
    return (
        gulp
            .src("./src/scss/*.scss")
            .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
            .pipe(cleanCSS())
            .pipe(autoPrefixer({
                browsers: [' > 0.1%']
            }))
            .pipe(rename(function(path){
                path.extname = '.min.css';
            }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest("./dist/css"))
    );
}

function optimizeImages() {
    return (
        gulp
            .src('./src/img/*.*')
            .pipe(imagemin())
            .pipe(gulp.dest('./dist/img/'))
    )
}

function scripts() {
    return (
        gulp
            .src('./src/js/*.js')
            .pipe(babel({
                presets: ['@babel/env']
            }))
            .pipe(terser())
            .pipe(rename(function(path){
                path.extname = '.min.js';
            }))
            .pipe(gulp.dest('./dist/js'))
    );
}

function copyHTML() {
    return (
        gulp
            .src('./src/index.html')
            .pipe(gulp.dest('./dist'))
    );
}

function serve() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch('./src/index.html',copyHTML).on('change',browserSync.reload);
    gulp.watch('./src/scss/**/*.scss', style);
}


gulp.task('style', style);
gulp.task('scripts',scripts);
gulp.task('copy-html', copyHTML);
gulp.task('serve', serve);


gulp.task('build', gulp.series(style,scripts, copyHTML));
gulp.task('dev', gulp.series(style, scripts, copyHTML, serve));
